package com.exampleDMSProject.demoDMSProject.createEnquiry.controller;

import com.exampleDMSProject.demoDMSProject.createEnquiry.ExcelGenerator;
import com.exampleDMSProject.demoDMSProject.createEnquiry.config.UserDetails;
import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.*;
import com.exampleDMSProject.demoDMSProject.createEnquiry.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController

@CrossOrigin(allowedHeaders = {"Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"},
        methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value = "/api")

public class CreateEnquiryController {
    @Autowired
    private CreateEnquiryRepo createEnquiryRepo;


    @Autowired
    private DealerBranchRepo dealerBranchRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private SalesOfficerRepo salesOfficerRepo;
    @Autowired
    private SourceRepo sourceRepo;
    @Autowired
    private OngoingEventRepo ongoingEventRepo;
    @Autowired
    private AnnualIncomRepo annualIncomRepo;
    @Autowired
    private OccupationRepo occupationRepo;
    @Autowired
    private SoilTypeConsistencyRepo soilTypeConsistencyRepo;
    @Autowired
    private CropsGrownRepo cropsGrownRepo;
    @Autowired
    private MakeRepo makeRepo;
    @Autowired
    private ModelRepo modelRepo;
    @Autowired
    private HpRepo hpRepo;

    @Autowired
    private PtoSpeedRepo ptoSpeedRepo;

    @Autowired
    private YearOfPurchaseRepo yearOfPurchaseRepo;

    private static final String MESSAGE = "msg";
    private static final String STATUS = "status";
    private static final String RESPONS = "respons";


    @PostMapping(value = "/addUser")
    public ResponseEntity<?> addUser(@RequestBody User1 registrationData) {

        ResponseEntity entity = null;
        Map<String, Object> map = new HashMap<>();

        User1 user1 = userRepo.findByEmail(registrationData.getEmail());
        if (user1 == null) {
            userRepo.save(registrationData);
            map.put("Message", "User registered successfully !!!");
            return new ResponseEntity<Object>(map, HttpStatus.CREATED);
        } else {
            map.put("Message", "Email alredy exist");
            return new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/userLogIn")
    public ResponseEntity<?> userLogIn(@RequestBody UserDetails userDetails) {

        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();

        User1 user1 = userRepo.findByUserName(userDetails.getUserName());

        if (user1 != null) {

            map.put(MESSAGE, "Login successfull");
        } else {
            map.put(MESSAGE, "Invalid Login");
        }

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getAllUser")
    public ResponseEntity<?> getAllUser() {

        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();

        List<User1> user1 = userRepo.getCallSP();
        map.put(MESSAGE, "List of all User");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, user1);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @PostMapping(value = "/addEnquiry")
    public ResponseEntity<?> saveEnquiry(@RequestBody CreateEnquiry createEnquiry) {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        CreateEnquiry enquiry = createEnquiryRepo.save(createEnquiry);
        map.put(MESSAGE, "Enquiry Added Successfully");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, enquiry);

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @PostMapping(value = "/addDealerBranch")
    public ResponseEntity<?> saveDealerBranch(@RequestBody DealerBranch dealerBranch) {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        DealerBranch dealerBranch1 = dealerBranchRepo.save(dealerBranch);
        map.put(MESSAGE, "DealerBranch Added Successfully");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, dealerBranch1);

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }


    @GetMapping(value = "/getAllDealerBranch")
    public ResponseEntity<?> getAllDealer() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<DealerBranch> dealerBranchList = dealerBranchRepo.findAll();
        map.put(MESSAGE, "List of Dealers");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, dealerBranchList);


        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getSalesOfficer")
    public ResponseEntity<?> getSalesOfficer() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<SalesOfficer> salesOfficerList = salesOfficerRepo.findAll();
        map.put(MESSAGE, "List of all salesofficers");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, salesOfficerList);

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getSource")
    public ResponseEntity<?> getSource() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<Source> sourceList = sourceRepo.findAll();
        map.put(MESSAGE, "List of all sales");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, sourceList);

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getOngoingEvent")
    public ResponseEntity<?> getOngoingEvent() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<OngoingEvent> ongoingEventList = ongoingEventRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, ongoingEventList);

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getAnnualIncome")
    public ResponseEntity<?> getAnnualIncome() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<AnnualIncome> annualIncomeList = annualIncomRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, annualIncomeList);

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getOccupation")
    public ResponseEntity<?> getOccupation() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<Occupation> occupations = occupationRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, occupations);

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getSoliType")
    public ResponseEntity<?> getSoliType() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<SoilTypeConsistency> type = soilTypeConsistencyRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, type);

        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getCropsGrown")
    public ResponseEntity<?> getCropsGrown() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<CropsGrown> cropsGrowns = cropsGrownRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, cropsGrowns);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getMake")
    public ResponseEntity<?> getMake() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<Make> makeList = makeRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, makeList);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getModel")
    public ResponseEntity<?> getModel() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<Model> modelList = modelRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, modelList);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getHp")
    public ResponseEntity<?> getHp() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<Hp> hplist = hpRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, hplist);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }


    @GetMapping(value = "/getYop")
    public ResponseEntity<?> getYop() {

        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<YearOfPurchase> yearOfPurchaseList = yearOfPurchaseRepo.findAll();

        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, yearOfPurchaseList);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }


    @GetMapping(value = "/getPtoSpeed")
    public ResponseEntity<?> getPtoSpeed() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<PtoSpeed> ptoSpeedList = ptoSpeedRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONS, ptoSpeedList);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/download/enquiryExcel.xlsx")
    public ResponseEntity<InputStreamResource> excelEnquiryReport() throws IOException {
        List<CreateEnquiry> enquiries = createEnquiryRepo.findAll();

        ByteArrayInputStream in = ExcelGenerator.enquiryToExcel(enquiries);
        // return IOUtils.toByteArray(in);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=enquiryExcel.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    @GetMapping(value = "/searchCreatedEnquiries")
    public ResponseEntity<?> searchCreatedEnquiries(
            @RequestParam() Long enquiryDateTo,
            @RequestParam() Long enquiryDateFrom,
            @RequestParam(required = false) Integer customerCode,
            @RequestParam(required = false) String village,
            @RequestParam(required = false) Long mobileNumber
    ) {
        Map<String, Object> map = new HashMap<>();
        List<CreateEnquiry> enquiriesList = createEnquiryRepo.searchCreatedEnquiries(
                enquiryDateFrom, enquiryDateTo, mobileNumber, customerCode, village
        );
        map.put("List", enquiriesList);
        map.put("success", HttpStatus.FOUND);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
