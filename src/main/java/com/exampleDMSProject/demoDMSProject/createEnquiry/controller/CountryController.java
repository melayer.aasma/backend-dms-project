package com.exampleDMSProject.demoDMSProject.createEnquiry.controller;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.*;
import com.exampleDMSProject.demoDMSProject.createEnquiry.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(allowedHeaders = {"Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"},
        methods = {RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(value = "/api")
public class CountryController {

    @Autowired
    private CountryRepo countryRepo;

    @Autowired
    private StateRepo stateRepo;

    @Autowired
    private DistrictRepo districtRepo;

    @Autowired
    private TalukaRepo talukaRepo;

    @Autowired
    private VillageRepo villageRepo;

    @PostMapping(value = "/addCountry")
    public Country saveCountry(@RequestBody Country country) {
        return countryRepo.save(country);
    }


    @PostMapping(value = "/addState")
    public State saveState(@RequestBody State state) {
        return stateRepo.save(state);
    }

    @PostMapping(value = "/addDistrict")
    public District saveDistrict(@RequestBody District district) {
        return districtRepo.save(district);
    }

    @PostMapping(value = "/addTaluka")
    public Taluka saveTaluka(@RequestBody Taluka taluka) {
        return talukaRepo.save(taluka);
    }

    @PostMapping(value = "/addVillage")
    public Village saveVillage(@RequestBody Village village) {
        return villageRepo.save(village);
    }


    @GetMapping(value = "/getAllCountry")
    public ResponseEntity<List<Country>> getCountryList() {

        List<Country> countries = countryRepo.getCountryList();
        return new ResponseEntity(countries, HttpStatus.OK);

    }

    @GetMapping(value = "/getState/{countryId}")
    public ResponseEntity<?> getStateList(@PathVariable Integer countryId) {

        ResponseEntity<Map<String, Object>> entity = null;
        List<State> states = stateRepo.getStateList(countryId);

        entity = new ResponseEntity(states, HttpStatus.OK);

        return entity;
    }


    @GetMapping(value = "/getDistrict/{stateId}")
    public ResponseEntity<?> getDistrictList(@PathVariable Integer stateId) {

        ResponseEntity<Map<String, Object>> entity = null;
        List<District> districts = districtRepo.getDistrictList(stateId);
        entity = new ResponseEntity(districts, HttpStatus.OK);

        return entity;
    }

    @GetMapping(value = "/getTaluka/{districtId}")
    public ResponseEntity<?> getTalukaList(@PathVariable Integer districtId) {

        ResponseEntity<Map<String, Object>> entity = null;
        List<Taluka> talukas = talukaRepo.getTalukaList(districtId);
        entity = new ResponseEntity(talukas, HttpStatus.OK);

        return entity;
    }

    @GetMapping(value = "/getVillage/{talukaId}")
    public ResponseEntity<?> getVillageList(@PathVariable Integer talukaId) {

        ResponseEntity<Map<String, Object>> entity = null;
        List<Village> villages = villageRepo.getVillageList(talukaId);
        entity = new ResponseEntity(villages, HttpStatus.OK);

        return entity;
    }


}
