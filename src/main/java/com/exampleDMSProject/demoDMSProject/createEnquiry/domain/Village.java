package com.exampleDMSProject.demoDMSProject.createEnquiry.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Data

public class Village {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer villageId;
    private String villageName;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "talukaId")

    private Taluka taluka;


}
