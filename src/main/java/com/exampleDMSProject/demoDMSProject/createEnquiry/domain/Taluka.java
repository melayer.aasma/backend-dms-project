package com.exampleDMSProject.demoDMSProject.createEnquiry.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Data
public class Taluka {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer talukaId;
    private String talukaName;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "districtId")
    private District district;


    @OneToMany(mappedBy = "taluka", cascade = CascadeType.ALL)
    @JsonManagedReference
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

    private Set<Village> villages;


}
