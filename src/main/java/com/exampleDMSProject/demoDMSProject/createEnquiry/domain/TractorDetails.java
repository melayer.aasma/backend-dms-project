package com.exampleDMSProject.demoDMSProject.createEnquiry.domain;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Data
public class TractorDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String make;
    private String model;
    private Integer hp;
    private Integer ptoSpeed;
    private Integer yearOfPurchase;


    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonBackReference
    @JoinColumn(name = "createEnquiryId")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private CreateEnquiry createEnquiry;

}
