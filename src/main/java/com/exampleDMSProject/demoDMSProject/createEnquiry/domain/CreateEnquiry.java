package com.exampleDMSProject.demoDMSProject.createEnquiry.domain;

import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.SearchEnquiry;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Data
public class CreateEnquiry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer createEnquiryId;

    @SequenceGenerator(name = "seq", initialValue = 1, allocationSize = 50)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer customerId;

    private Integer customerCode;

    private Long enquiryDate;

    private String dealerBranchOrOutlet;

    private String salesOfficer;

    private String source;

    private String sourceDetails;

    private String ongoingEvent;

    private Long mobileNumber;

    private String fullName;

    private String fatherName;


    private String country;

    private String state;

    private String district;

    private String taluka;

    private String village;

    private String email;

    private Long alternateMobileNumber;


    private Integer pinCode;

    private Long dateOfBirth;

    private Integer age;


    private Long annualIncome;


    private String occupation;


    private Integer landSize;


    private String soilTypeConsistency;


    private String cropsGrown;

    @OneToMany(mappedBy = "createEnquiry", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<TractorDetails> tractorDetails;

}
