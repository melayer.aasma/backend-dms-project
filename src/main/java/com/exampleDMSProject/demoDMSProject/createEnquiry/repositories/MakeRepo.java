package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.Make;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MakeRepo extends JpaRepository<Make, Integer> {
}
