package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.Hp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HpRepo extends JpaRepository<Hp, Integer> {
}
