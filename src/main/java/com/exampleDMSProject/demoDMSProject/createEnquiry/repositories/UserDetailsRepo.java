package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.config.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDetailsRepo extends JpaRepository<UserDetails,Integer> {


}
