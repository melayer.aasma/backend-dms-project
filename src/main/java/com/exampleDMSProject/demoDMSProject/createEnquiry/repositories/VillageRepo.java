package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.Village;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VillageRepo extends JpaRepository<Village, Integer> {
    @Query(value = "select * from village where taluka_id=?", nativeQuery = true)
    List<Village> getVillageList(Integer talukaId);
}
