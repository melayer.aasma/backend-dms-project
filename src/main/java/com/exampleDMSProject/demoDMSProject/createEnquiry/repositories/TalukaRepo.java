package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.Taluka;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TalukaRepo extends JpaRepository<Taluka, Integer> {

    @Query(value = "select * from taluka where district_id=?", nativeQuery = true)
    List<Taluka> getTalukaList(Integer districtId);
}
