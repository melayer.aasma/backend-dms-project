package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.Occupation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OccupationRepo extends JpaRepository<Occupation,Integer> {
}
