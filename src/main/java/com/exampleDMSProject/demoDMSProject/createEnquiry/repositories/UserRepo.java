package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.User1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<User1, Integer> {

    User1 findByEmail(String email);

    User1 findByUserName(String userName);

    @Query(value = "{call getAllUser}", nativeQuery = true)
    List<User1> getCallSP();

}
