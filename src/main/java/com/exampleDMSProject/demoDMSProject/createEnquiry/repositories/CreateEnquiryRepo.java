package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.CreateEnquiry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreateEnquiryRepo extends JpaRepository<CreateEnquiry, Integer> {

    //    @Query(value = "select * from create_enquiry where customerCode=? ,mobileNumber=? ", nativeQuery = true)
    List<CreateEnquiry> findByCustomerCodeAndMobileNumber(Integer customerCode, Long mobileNumber);

//    List<CreateEnquiry> finbByEnquiryDateFromAndEnquiryDateTo(Long enquiryDateFrom, Long enquiryDateTo);


    @Query(value = "{call sp_searchEnquir(:enquiryDateFrom,:enquiryDateTo,:mobileNumber,:customerCode,:village)}", nativeQuery = true)
    List<CreateEnquiry> searchCreatedEnquiries(
            @Param("enquiryDateFrom") Long enquiryDateFrom,
            @Param("enquiryDateTo") Long enquiryDateTo,
            @Param("mobileNumber") Long mobileNumber,
            @Param("customerCode") Integer customerCode,
            @Param("village") String village);
}
