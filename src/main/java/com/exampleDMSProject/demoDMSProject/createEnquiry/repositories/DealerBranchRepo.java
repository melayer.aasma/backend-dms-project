package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.DealerBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface DealerBranchRepo extends JpaRepository<DealerBranch,Integer> {


    List<DealerBranch> findAll();

    DealerBranch save(DealerBranch dealerBranch);
}
