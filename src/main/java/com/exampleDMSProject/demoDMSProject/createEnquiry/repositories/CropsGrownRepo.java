package com.exampleDMSProject.demoDMSProject.createEnquiry.repositories;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.CropsGrown;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CropsGrownRepo extends JpaRepository<CropsGrown,Integer> {
}
