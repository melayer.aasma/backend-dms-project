package com.exampleDMSProject.demoDMSProject.view;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.CreateEnquiry;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ExcelView extends AbstractXlsView {


    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {


        try {

            // change the file name
            response.setHeader("Content-Disposition", "attachment; filename=\"my-xls-file.xls\"");

            @SuppressWarnings("unchecked")
            List<CreateEnquiry> enquiry = (List<CreateEnquiry>) model.get("enquiry");
            // create excel xls sheet
            Sheet sheet = workbook.createSheet("Enquiry Details");
            sheet.setDefaultColumnWidth(30);

            // create style for header cells
            CellStyle style = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setFontName("Arial");
            style.setFillForegroundColor(HSSFColor.BLUE.index);
//            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//            font.setBold(true);
            font.setColor(HSSFColor.WHITE.index);
            style.setFont(font);


            // create header row
            Row header = sheet.createRow(0);
            header.createCell(0).setCellValue("Index");
            header.getCell(0).setCellStyle(style);
            header.createCell(1).setCellValue("Mobile Number");
            header.getCell(1).setCellStyle(style);
            header.createCell(2).setCellValue("Name of user");
            header.getCell(2).setCellStyle(style);
            header.createCell(3).setCellValue("Email");
            header.getCell(3).setCellStyle(style);

            int rowCount = 1;

            for (CreateEnquiry  enquiry1 : enquiry) {
                Row userRow = sheet.createRow(rowCount++);
                userRow.createCell(0).setCellValue(enquiry1.getCreateEnquiryId());
                userRow.createCell(1).setCellValue(enquiry1.getCustomerId());
                userRow.createCell(2).setCellValue(enquiry1.getCustomerCode());
                userRow.createCell(3).setCellValue(enquiry1.getEnquiryDate());

            }
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }
}
