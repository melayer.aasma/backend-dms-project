package com.exampleDMSProject.demoDMSProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDmsProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDmsProjectApplication.class, args);
	}

}

