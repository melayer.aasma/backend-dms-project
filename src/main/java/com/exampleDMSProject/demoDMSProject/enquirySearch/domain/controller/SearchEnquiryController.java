package com.exampleDMSProject.demoDMSProject.enquirySearch.domain.controller;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.CreateEnquiry;
import com.exampleDMSProject.demoDMSProject.createEnquiry.repositories.CreateEnquiryRepo;
import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.EnquiryStatus;
import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.EnquiryType;
import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.Product;
import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.SearchEnquiry;
import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.repository.EnquiryStatusRepo;
import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.repository.EnquiryTypeRepo;
import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.repository.ProductRepo;
import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.repository.SearchEnquiryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api")
public class SearchEnquiryController {

    @Autowired
    private SearchEnquiryRepo searchEnquiryRepo;
    @Autowired
    private EnquiryStatusRepo enquiryStatusRepo;
    @Autowired
    private EnquiryTypeRepo enquiryTypeRepo;
    @Autowired
    private ProductRepo productRepo;
@Autowired
private CreateEnquiryRepo createEnquiryRepo;



    private static final String MESSAGE = "msg";
    private static final String STATUS = "status";
    private static final String RESPONSE = "respons";


    @PostMapping(value = "/addEnquiryType")
    public ResponseEntity<?> addEnquirytype(@RequestBody EnquiryType enquiryType) {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        EnquiryType type = enquiryTypeRepo.save(enquiryType);
        map.put(MESSAGE, "added type ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONSE, type);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @PostMapping(value = "/addEnquirytype")
    public ResponseEntity<?> addEnquirytype(@RequestBody EnquiryStatus enquiryStatus) {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        EnquiryStatus status = enquiryStatusRepo.save(enquiryStatus);
        map.put(MESSAGE, "added type ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONSE, status);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }
    @PostMapping(value = "/addProduct")
    public ResponseEntity<?> addProduct(@RequestBody Product product) {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        Product product1 = productRepo.save(product);
        map.put(MESSAGE, "added product ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONSE, product1);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getEnquirytype")
    public ResponseEntity<?> getEnquirytype() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<EnquiryType> typeList = enquiryTypeRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONSE, typeList);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }


    @GetMapping(value = "/getEnquiryStatus")
    public ResponseEntity<?> getEnquiryStatus() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<EnquiryStatus> statusList = enquiryStatusRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONSE, statusList);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

    @GetMapping(value = "/getAllProduct")
    public ResponseEntity<?> getAllProduct() {
        ResponseEntity<Map<String, Object>> entity = null;
        Map<String, Object> map = new HashMap<>();
        List<Product> productList = productRepo.findAll();
        map.put(MESSAGE, "List of all ");
        map.put(STATUS, HttpStatus.OK);
        map.put(RESPONSE, productList);
        entity = new ResponseEntity(map, HttpStatus.OK);
        return entity;
    }

//
//    @GetMapping(value = "/searchForEnquiry/{customerCode}/{mobileNumber}")
//    public ResponseEntity<?> searchEnquiry(@PathVariable Integer customerCode,
//                                            @PathVariable Long mobileNumber){
//
//        ResponseEntity<Map<String,Object>> entity=null;
//        Map<String,Object> map=new HashMap<>();
//
//        List<CreateEnquiry> list=createEnquiryRepo.findByCustomerCodeAndMobileNumber(customerCode,mobileNumber);
//        map.put(MESSAGE,"List of enquiry");
//        map.put(STATUS,HttpStatus.OK);
//        map.put(RESPONSE,list);
//        entity=new ResponseEntity(map, HttpStatus.OK);
//        return entity;
//
//    }

//
//    @PostMapping(value = "/saveSearchEnquiry")
//    public ResponseEntity<?> saveSearchEnquiry(@RequestBody SearchEnquiry searchEnquiry){
//
//        ResponseEntity<Map<String,Object>> entity=null;
//        Map<String,Object> map=new HashMap<>();
//
//       SearchEnquiry searchEnquiry1=searchEnquiryRepo.save(searchEnquiry);
//        map.put(MESSAGE,"Enquiry saved");
//        map.put(STATUS,HttpStatus.OK);
//        map.put(RESPONSE,searchEnquiry1);
//        entity=new ResponseEntity(map, HttpStatus.OK);
//        return entity;
//
//    }
//
//    @GetMapping(value = "/sesarchByDateFrom&To/{enquiryDateFrom}/{enquiryDateTo}")
//    public ResponseEntity<?> searchEnquiryByDateFromTo(@PathVariable Long enquiryDateFrom,@PathVariable Long enquiryDateTo ){
//
//        ResponseEntity<Map<String,Object>> entity=null;
//        Map<String,Object> map=new HashMap<>();
//
//        List<SearchEnquiry> listEnquiry=searchEnquiryRepo.findByEnquiryDateFromAndEnquiryDateTo(enquiryDateFrom,enquiryDateTo);
//        map.put(MESSAGE,"List of enquiry");
//        map.put(STATUS,HttpStatus.OK);
//        map.put(RESPONSE,listEnquiry);
//        entity=new ResponseEntity(map, HttpStatus.OK);
//        return entity;
//
//    }





}
