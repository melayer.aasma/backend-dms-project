package com.exampleDMSProject.demoDMSProject.enquirySearch.domain.repository;

import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProductRepo extends JpaRepository<Product,Integer> {
    List<Product> findAll();
}
