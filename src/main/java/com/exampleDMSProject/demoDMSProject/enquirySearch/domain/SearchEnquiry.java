package com.exampleDMSProject.demoDMSProject.enquirySearch.domain;

import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.CreateEnquiry;
import com.exampleDMSProject.demoDMSProject.createEnquiry.domain.TractorDetails;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Data
public class SearchEnquiry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer enquiryNumber;
    private String enquiryType;
    private String enquiryStatus;
    private String product;
    private String prospectName;
    @NotNull
    private Long enquiryDateFrom;
    @NotNull
    private Long enquiryDateTo;
    private Long followUpDate;
    private String village;
    private String tahsil;
    private String reason;
    private String remarksByTelecaller;
    private Long mobileNumber;
    private Long expectedDeliveryDate;
    private Integer customerCode;

//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "createEnquiry_id", nullable = false)
//    private CreateEnquiry createEnquiry;




}
