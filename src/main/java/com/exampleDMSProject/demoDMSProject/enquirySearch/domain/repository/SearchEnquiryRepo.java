package com.exampleDMSProject.demoDMSProject.enquirySearch.domain.repository;

import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.SearchEnquiry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchEnquiryRepo extends JpaRepository<SearchEnquiry,Integer> {


    List<SearchEnquiry> findByEnquiryDateFromAndEnquiryDateTo(Long enquiryDateFrom, Long enquiryDateTo);
}
