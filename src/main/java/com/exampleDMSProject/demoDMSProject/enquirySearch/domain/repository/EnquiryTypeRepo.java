package com.exampleDMSProject.demoDMSProject.enquirySearch.domain.repository;

import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.EnquiryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface EnquiryTypeRepo extends JpaRepository<EnquiryType,Integer> {

}
