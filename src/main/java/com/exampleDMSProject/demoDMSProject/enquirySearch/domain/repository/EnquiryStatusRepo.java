package com.exampleDMSProject.demoDMSProject.enquirySearch.domain.repository;

import com.exampleDMSProject.demoDMSProject.enquirySearch.domain.EnquiryStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface EnquiryStatusRepo extends JpaRepository<EnquiryStatus,Integer> {

}
